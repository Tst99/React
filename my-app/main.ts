import Knex from 'knex';
import dotenv from 'dotenv'
dotenv.config()
import {Request,Response} from 'express';
import express from 'express';
const knexConfigs = require('./knexfile');
const configMode = process.env.NODE_ENV || "development";
const knexConfig = knexConfigs[configMode]
const knex = Knex(knexConfig)

// const studentService = new StudentService(knex);
const app = express();