import React, { useEffect, useState } from 'react'
import { Button, Table, Modal} from 'antd'
import axios from 'axios'
import { DeleteOutlined, EditOutlined, ExclamationCircleOutlined } from '@ant-design/icons';
import { Link } from 'react-router-dom';
const { confirm } = Modal


export default function NewsPublish(props) {
  const columns = [
    {
        title: '新聞標題',
        dataIndex: 'title',
        render: (title,item) => {
          return <Link to={`/news-manage/preview/${item.id}`}>{title}</Link>
        }
      },
    {
      title: '作者',
      dataIndex: 'author',
    },
    {
        title: '新聞分類',
        dataIndex: 'category',
        render: (category) => {
          return <div>{category.title}</div>
        }
      },
    {
      title: "操作",
      render: (item) => {
        
        return <div style={{ display: 'flex', flexDirection: 'row' }}>
          {props.button(item)}

        </div>
      }
    }
  ];

  return (
    <div>
      <Table dataSource={props.dataSource} columns={columns}
        pagination={{
          pageSize: 5
        }}
        rowKey={item=>item.id}
      />
    </div>
  )
}
