
import { notification } from 'antd'
import axios from 'axios'
import React, { useEffect, useState } from 'react'

function usePublish(publishState) {
    const [dataSource, setdataSource] = useState([])
    const { username } = JSON.parse(localStorage.getItem("token"))
    useEffect(() => {
        axios.get(`/news?author=${username}&publishState=${publishState}&_expand=category`).then(res => {
            setdataSource(res.data)
        })
    }, [username, publishState])

    const handlePublish = (data) => {
        console.log(data.id)
        setdataSource(dataSource.filter(item => item.id !== data.id))
        axios.patch(`/news/${data.id}`, {
            publishState: 2,
            "publishTime": Date.now()
        }).then(res => {
            notification.info({
                message: `通知`,
                description: '你可以到' + '【發佈管理/已發佈】' + '中查看你的新聞',
                placement: "bottomRight",
            })
        })
    }

    const handleSunset = (data) => {
        setdataSource(dataSource.filter(item => item.id !== data.id))
        axios.patch(`/news/${data.id}`, {
            publishState: 3,
        }).then(res => {
            notification.info({
                message: `通知`,
                description: '你可以到' + '【發佈管理/已下線】' + '中查看你的新聞',
                placement: "bottomRight",
            })
        })
    }

    const handleDelete = (data) => {
        setdataSource(dataSource.filter(item => item.id !== data.id))
        axios.delete(`/news/${data.id}`).then(res => {
            notification.info({
                message: `通知`,
                description: '你已經刪除了已下線的新聞',
                placement: "bottomRight",
            })
        })
    }
    return {
        dataSource,
        handleDelete,
        handlePublish,
        handleSunset
    }
}

export default usePublish