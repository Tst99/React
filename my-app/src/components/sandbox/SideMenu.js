import React, { useEffect, useState } from 'react'
import "./index.css"
import { Layout, Menu } from 'antd';
import { ContainerOutlined, UserOutlined, SmileTwoTone, AlibabaOutlined } from '@ant-design/icons';
import { useNavigate, useLocation } from "react-router-dom";
import axios from 'axios';
import { connect } from 'react-redux';
const { Sider } = Layout;

const iconList = {
  "/home": <SmileTwoTone />,
  "/user-manage": <UserOutlined />,
  "/user-manage/list": <UserOutlined />,
  '/right-manage': <UserOutlined />,
  '/news-manage': <UserOutlined />,
  '/audit-manage': <UserOutlined />,
  '/publish-manage': <UserOutlined />,
  //....
}

function SideMenu(props) {
  //useLocation
  let navigate = useNavigate()
  let location = useLocation();
  let openKeys = ["/" + location.pathname.split("/")[1]]
  // console.log(openKeys)
  // Use state 
  const [menu, setMenu] = useState([])
  // console.log("menu : ",menu)

  function checkPermisson(data, rights) {
    // console.log(data,rights)
    if (data.pagepermisson == 1 && rights.includes(data.key)) {
      return true
    }
  }

  function getItem(data, rights = []) {
    let ans = []
    let icon = null
    // console.log(rights)
    for (let x in data) {
      // check pagepermisson
      if (!checkPermisson(data[x], rights)) {
        continue
      }
      // add icon
      if (iconList.hasOwnProperty(data[x].key)) {
        icon = iconList[data[x].key]
      }

      if (data[x].hasOwnProperty("children") == false || data[x].children.length == 0) {
        ans.push({ label: data[x].title, key: data[x].key, pagepermisson: data[x].pagepermisson, grade: data[x].grade, icon: icon })
        icon = null
      } else {
        let child = getItem(data[x].children, rights)
        ans.push({ label: data[x].title, key: data[x].key, children: child, pagepermisson: data[x].pagepermisson, grade: data[x].grade, icon: icon })
        icon = null
      }
    }
    return ans
  }

  const { role: { rights } } = JSON.parse(localStorage.getItem("token"))
  useEffect(() => {
    axios.get("/rights?_embed=children").then(res => {
      setMenu(getItem(res.data, rights))
    })
  }, [])





  const onClick = (e) => {
    console.log('click ', e);
    navigate(e.key)
  };

  return (
    <Sider trigger={null} collapsible collapsed={props.isCollapsed}>
      <div style={{ display: "flex", height: "100%", flexDirection: "column" }}>
        <div className="logo" > <AlibabaOutlined />新聞發布系統 </div>
        <div style={{ flex: 1, "overflow": "auto" }}>
          <Menu
            onClick={onClick}
            theme="dark"
            mode="inline"
            selectedKeys={location.pathname}
            defaultOpenKeys={openKeys}
            items={menu}
          />
        </div>
      </div>
    </Sider>
  )
}


const mapStateToProps = ({ CollapsedReducer: { isCollapsed } }) => {
  return {
    isCollapsed: isCollapsed
  }
}



export default connect(mapStateToProps)(SideMenu)