import React, { useEffect, useState } from 'react';
import {
  UserOutlined,
  MenuFoldOutlined,
  MenuUnfoldOutlined,
} from '@ant-design/icons';
import { Layout, Dropdown, Menu, Avatar } from 'antd';
import { } from '@ant-design/icons';
import { useNavigate, useLocation } from "react-router-dom";
import {connect} from "react-redux"
const { Header } = Layout;



function TopHeader(props) {
  // const [collapsed, setCollapsed] = useState(false)
  const{change} = props
  // console.log("TopHeader",props)

  const { role: { roleName }, username } = JSON.parse(localStorage.getItem("token"))

  let navigate = useNavigate();
  const changeCollapsed = () => {
    // setCollapsed(!collapsed)
    change()
  }

  const menu = (
    <Menu
      items={[
        {
          key: '1',
          label: roleName,
        },
        {
          key: '4',
          danger: true,
          label: 'Logout',
          onClick: () => {
            localStorage.removeItem("token");
            navigate("/login")
          }
        },
      ]}
    />
  );

  return (
    <Header className="site-layout-background" style={{ padding: '0 16px' }}>
      {
        props.isCollapsed ?
          <MenuUnfoldOutlined onClick={changeCollapsed} /> :
          <MenuFoldOutlined onClick={changeCollapsed} />
      }
      <div style={{ float: "right" }}>
        <span>歡迎
          <span style={{ color: "#1890ff" }}>{username}</span>
          回來
        </span>
        <Dropdown overlay={menu}>
          <Avatar
            style={{
              backgroundColor: '#87d068',
            }}
            icon={<UserOutlined />}
          />
        </Dropdown>
      </div>
    </Header>
  )
}
/*
connect(
//mapStateToProps
//mapDispatchToProps
)
*/


const changeCollapsed = () => {
  return {
    type : "CHANGE"
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    change: () => dispatch(changeCollapsed())
  }
}

const mapStateToProps = ({CollapsedReducer:{isCollapsed}}) =>{
  // console.log(isCollapsed)
  return{
    isCollapsed:isCollapsed
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(TopHeader)