export const CollapsedReducer = (prevState={
    isCollapsed:false
},action)=>{
    // console.log(action)
    switch (action.type) {
        case 'CHANGE':
            return {
               isCollapsed : !prevState.isCollapsed
            }
        default:
            return prevState
    }
}