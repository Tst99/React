export const LoadingReducer = (prevState={
    isLoading:false
},action)=>{
    // console.log(action)
    switch (action.type) {
        case 'change_loading':
            let newstate = {...prevState}
            newstate.isLoading = action.payload
            return newstate
        default:
            return prevState
    }
}
