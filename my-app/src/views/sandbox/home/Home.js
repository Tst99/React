import React, { useEffect, useRef, useState } from 'react'
import { Button, Card, Col, List, Row, Avatar, Drawer } from 'antd';
import axios from 'axios'
import Item from 'antd/lib/list/Item';
import { Link, useNavigate } from 'react-router-dom';
import { EditOutlined, EllipsisOutlined, SettingOutlined } from '@ant-design/icons';
import * as Echarts from "echarts"
import _ from 'lodash';
import { object } from 'prop-types';
const { Meta } = Card;


export default function Home() {
  const [dataSource, setdataSource] = useState([])
  const [likeSource, setlikeSource] = useState([])
  const [allList,setallList] = useState([])
  const [visible, setvisible] = useState(false)
  const [pieChart,setpieChart] = useState(null)

  const pieRef = useRef()
  const barRef = useRef()

  useEffect(() => {
    axios.get("/news?publishState=2&_expand=category&_sort=view&_order=desc&_limit=6").then(res => {
      setdataSource(res.data)

    })
  }, [])

  useEffect(() => {
    axios.get("/news?publishState=2&_expand=category&_sort=star&_order=desc&_limit=6").then(res => {
      setlikeSource(res.data)
    })
  }, [])

  useEffect(() => {
    axios.get("/news?publishState=2&_expand=category").then(res => {
      // console.log(res.data)
      // console.log(_.groupBy(res.data,item=>item.category.title))
      renderBarView(_.groupBy(res.data, item => item.category.title))
      setallList(res.data)
    })
    return () => {
      console.log("cancel")
      window.onresize = null
    }
  }, [])

  const renderBarView = (obj) => {
    var myChart = Echarts.init(barRef.current);
    var option = {
      title: {
        text: '新聞分類圖示'
      },
      tooltip: {},
      legend: {
        data: ['數量']
      },
      xAxis: {
        data: Object.keys(obj),
        axisLabel: {
          rotate: 15,
          interval: 0
        }
      },
      yAxis: {
        minInterval: 1
      },
      series: [
        {
          name: '數量',
          type: 'bar',
          data: Object.values(obj).map(item => item.length)
        }
      ]
    };
    myChart.setOption(option);


    window.onresize = () => {
      console.log("resize")
      myChart.resize()
    }
  }

  const renderpieView = (obj) => {
    var currentList = allList.filter(item=>item.author === username )
    var groupObj = _.groupBy(currentList,item=>item.category.title)
    // console.log(groupObj)
    // console.log(currentList)

    var list = []
    for(var i in groupObj){
      list.push({
        name:i,
        value:groupObj[i].length
      })
    }
    console.log(list)

    var myChart;
    if (!pieChart){
      myChart = Echarts.init(pieRef.current);
      setpieChart(myChart)
    } else{
      myChart = pieChart
    }
    var option;

    option = {
      title: {
        text: '當前用戶新聞分類圖示',
        subtext: '',
        left: 'center'
      },
      tooltip: {
        trigger: 'item'
      },
      legend: {
        orient: 'vertical',
        left: 'left'
      },
      series: [
        {
          name: '發佈數量',
          type: 'pie',
          radius: '50%',
          data: list,
          emphasis: {
            itemStyle: {
              shadowBlur: 10,
              shadowOffsetX: 0,
              shadowColor: 'rgba(0, 0, 0, 0.5)'
            }
          }
        }
      ]
    };

    option && myChart.setOption(option);
  }

  const { username, region, role: { roleName } } = JSON.parse(localStorage.getItem("token"))

  return (
    <div className="site-card-wrapper">
      <Row gutter={16}>
        <Col span={8}>
          <Card title="用戶最常瀏覽" bordered={true}>
            <List
              size='small'
              // bordered
              dataSource={dataSource}
              renderItem={item => <List.Item><Link to={`/news-manage/preview/${item.id}`}>{item.title}</Link></List.Item>}
            ></List>
          </Card>
        </Col>
        <Col span={8}>
          <Card title="用戶點讚最多" bordered={true}>
            <List
              size='small'
              // bordered
              dataSource={likeSource}
              renderItem={item => <List.Item><Link to={`/news-manage/preview/${item.id}`}>{item.title}</Link></List.Item>}
            ></List>
          </Card>
        </Col>
        <Col span={8}>
          <Card
            cover={
              <img
                alt="example"
                src="/home.png"
              />
            }
            actions={[
              <SettingOutlined key="setting" onClick={async() => {
                  await setvisible(true)
                  await renderpieView()
              }} />,
              // <EditOutlined key="edit" />,
              // <EllipsisOutlined key="ellipsis" />,
            ]}
          >
            <Meta
              // avatar={<Avatar src="https://joeschmoe.io/api/v1/random" />}
              title={username}
              description={
                <div>
                  <b>{region ? region : "全球"}</b>
                  <span style={{ paddingLeft: "30px" }}>{roleName}</span>
                </div>
              }
            />
          </Card>
        </Col>
      </Row>

      <Drawer width="500px" title="個人新聞分類" placement="right" onClose={() => setvisible(false)} visible={visible}>
        <div ref={pieRef} style={{ height: "400px", margin: "30px", "width": "100%" }}></div>
      </Drawer>
      <div ref={barRef} style={{ height: "400px", margin: "30px", "width": "100%" }}></div>

    </div>
  )
}








    // Get
    // axios.get("http://localhost:8000/posts").then(res=>{
    //   console.log(res.data)
    // })

    // Post
    // axios.post("http://localhost:8000/posts",{
    //   title:"33333",
    //   author:"Me"
    // })

    // Put
    // axios.put("http://localhost:8000/posts/1",{
    //   title:"1111-put"
    // })

    // Patch
    // axios.patch("http://localhost:8000/posts/1",{
    //   title:"1111-put-11"
    // })

    // Delete
    // axios.delete("http://localhost:8000/posts/1")

    // _embed      ID -> reference ID
    // axios.get("http://localhost:8000/posts?_embed=comments").then(res=>{
    //   console.log(res.data)
    // })

    // _expand     reference ID -> ID
    // axios.get("http://localhost:8000/comments?_expand=post").then(res=>{
    //   console.log(res.data)
    // })