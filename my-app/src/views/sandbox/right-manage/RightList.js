import React, { useEffect, useState } from 'react'
import { Button, Table, Tag, Modal, Popover, Switch } from 'antd'
import axios from 'axios'
import { DeleteOutlined, EditOutlined, ExclamationCircleOutlined } from '@ant-design/icons';
const { confirm } = Modal


export default function RightList() {
  // console.log('rendered')
  const [dataSource, setdataSource] = useState([])

  useEffect(() => {
    axios.get('/rights?_embed=children').then(res => {
      const list = res.data
      list.forEach(item => {
        if (item.children.length === 0) {
          item.children = ""
        }
      })
      // console.log(res.data)
      setdataSource(list)
    })
  }, [])

  const columns = [
    {
      title: 'ID',
      dataIndex: 'id',
      render: (id) => {
        return <div>{id}</div>
      }
    },
    {
      title: '權限',
      dataIndex: 'title',
    },
    {
      title: '權限路徑',
      dataIndex: 'key',
      render: (route) => {
        return <Tag color='orange'>{route}</Tag>
      }
    },
    {
      title: "操作",
      render: (item) => {
        
        return <div style={{ display: 'flex', flexDirection: 'row' }}>
          <Button danger shape="circle" icon={<DeleteOutlined />}
            onClick={() => confirmMethod(item)} />
          <div style={{ padding: 5 }}></div>
          <Popover content={<div style={{ textAlign: "center" ,fontSize: 6}}>
            (刷新頁面後生效)
            <br></br>
            <Switch checked={item.pagepermisson} onChange={()=>switchMethod(item)}></Switch>
          </div>} title="是否顯示在左側欄" trigger={
            item.pagepermisson == undefined ? "" : 'click'
          }>
            <Button type="primary" shape="circle" icon=
              {<EditOutlined />} disabled={item.pagepermisson === undefined} />
          </Popover>
        </div>
      }
    }
  ];

  const switchMethod = (item) =>{
    item.pagepermisson = item.pagepermisson === 1?0:1
    console.log(item)
    setdataSource([...dataSource])
    if(item.grade===1){
      axios.patch(`/rights/${item.id}`,{
        pagepermisson:item.pagepermisson
      })
    }else{
      axios.patch(`/children/${item.id}`,{
        pagepermisson:item.pagepermisson
      })
    }

  }

  const confirmMethod = (item) => {
    confirm({
      title: '確定刪除嗎?',
      icon: <ExclamationCircleOutlined />,
      content: '該操作不可挽回!',
      onOk() {
        deleteMethod(item)
        // console.log('OK');
      },
      onCancel() {
      },
    });
  }

  const deleteMethod = (item) => {
    // console.log(item)
    // console.log(dataSource)
    if (item.grade === 1) {
      setdataSource(dataSource.filter(data => data.id !== item.id))
      axios.delete(`/rights/${item.id}`)
    } else {
      let list = dataSource.filter(data => data.id == item.rightId)
      list[0].children = list[0].children.filter(data => data.id != item.id)
      setdataSource([...dataSource])
      axios.delete(`/children/${item.id}`)
    }
  }

  return (
    <div>
      <Table dataSource={dataSource} columns={columns}
        pagination={{
          pageSize: 5
        }}
      />
    </div>
  )
}
