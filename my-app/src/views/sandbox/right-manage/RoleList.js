import { Table, Button, Modal ,Tree} from 'antd'
import React, { useEffect, useState } from 'react'
import axios from 'axios'
import { DeleteOutlined, EditOutlined, ExclamationCircleOutlined } from '@ant-design/icons';
const { confirm } = Modal

export default function RoleList() {
  const [dataSource, setdataSource] = useState([])
  const [rightList, setRightList] = useState([])
  const [currentRights,setcurrentRights]=useState([])
  const [currentId,setcurrentId] = useState([])
  const [isModalVisible, setisModalVisible] = useState(false)
  const columns = [
    {
      title: 'ID',
      dataIndex: 'id',
      render: (id) => {
        return <b>{id}</b>
      }
    },
    {
      title: '用戶名稱',
      dataIndex: 'roleName',
    },
    {
      title: "操作",
      render: (item) => {
        return <div>
          {/* <Button danger shape="circle" icon={<DeleteOutlined />}
          // onClick={(item)=>confirmMethod(item)}
          /> */}
          <Button type="primary" shape="circle" icon={<EditOutlined />} onClick={() => {
            setcurrentRights(item.rights)
            setcurrentId(item.id)
            setisModalVisible(true)
            // console.log(item)
            // console.log(currentRights)
          }} />
        </div>
      }
    }
  ]
  useEffect(() => {
    axios.get("/roles").then(res => {
      // console.log("dataSource : ",res.data)
      setdataSource(res.data)
    })
  }, [])

  useEffect(() => {
    axios.get("/rights?_embed=children").then(res => {
      // console.log("RightList : ",res.data)
      setRightList(res.data)
    })
  }, [])

  const handleOk = () => {
    console.log(currentRights)
    setisModalVisible(false)
    setdataSource(dataSource.map(item=>{
      if(item.id === currentId){
        return{
          ...item,
          rights:currentRights
        }
      }
      return item
    }))
    axios.patch(`/roles/${currentId}`,{
      rights:currentRights
    })
  }

  const handleCancel = () => {
    setisModalVisible(false)
  }
  const onCheck = (checkKeys)=>{
    // console.log(checkKeys.checked)
    setcurrentRights(checkKeys.checked)
  }
  return (
    
    <div>
      <Table dataSource={dataSource} columns={columns} rowKey={(item) => item.id} />
      <Modal title="權限控制" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
        <Tree
          checkable
          onCheck={onCheck}
          checkStrictly = {true}
          checkedKeys = {currentRights}
          treeData={rightList}
        />
      </Modal>
    </div>
  )
}
