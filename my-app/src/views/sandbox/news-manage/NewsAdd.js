import React, { useEffect, useState, useRef } from 'react'
import { Button, PageHeader, Steps, Form, Input, Select, message, notification } from 'antd'
import style from "./News.module.css"
import axios from "axios"
import NewsEditor from '../../../components/news-manage/NewsEditor';
import { useNavigate } from "react-router-dom";

const { Option } = Select;
const { Step } = Steps

export default function NewsAdd(){
  let navigate = useNavigate()
  const [current, setCurrent] = useState(0)
  const [categoryList, setcategoryList] = useState([])

  const [formInfo, setformInfo] = useState({})
  const [content,setContent] = useState("")

  const User =JSON.parse(localStorage.getItem("token"))

  const handleNext = () => {
    if (current === 0) {
      NewsForm.current.validateFields().then(res=>{
        setformInfo(res)
        // console.log(res)
        setCurrent(current + 1)
      }).catch(error=>{
        console.log(error)
      })
    } else {
      console.log("form : ",formInfo,"  content : ",content)
      if(content===""||content.trim()==="<p></p>"){
        message.error("新聞內容不得為空")
      }else{
        setCurrent(current + 1)
      }
    }
  }
  const handlePrevious = () => {
    setCurrent(current - 1)
  }

  const NewsForm = useRef(null)

  useEffect(() => {
    axios.get("/categories").then(res => {
      console.log(res.data)
      setcategoryList(res.data)
    })
  }, [])

  const handleSave = (auditState)=>{
    axios.post('/news',{
      ...formInfo,
      "content":content,
      "region":User.region?User.region:"全球",
      "author":User.username,
      "roleId":User.roleId,
      "auditState":auditState,
      "publishState":0,
      "createTime":Date.now(),
      "star":0,
      "view":0,
      // "pulishTime":0
    }).then(res=>{
      navigate(auditState===0?"/news-manage/draft":"/audit-manage/list")
      notification.info({
        message:`通知`,
        description:`你可以到${auditState===0?'草稿箱':'審核列表'}中查看您的新聞`,
        placement:"bottomRight",
      })
    })
  }

  return (
    <div>
      <PageHeader
        className='site-page-header'
        title="撰寫新聞"
        subTitle=""
      ></PageHeader>
      <Steps current={current}>
        <Step title="基本信息" description="新聞標題, 新聞分類" />
        <Step title="新聞內容" description="新聞主體內容" />
        <Step title="新聞提交" description="保存草稿或者提交審核" />
      </Steps>

      <div style={{ marginTop: "50px" }}>
        <div className={current === 0 ? '' : style.active}>
          <Form
            name="basic"
            labelCol={{ span: 4 }}
            wrapperCol={{ span: 20 }}
            initialValues={{ remember: true }}
            onFinish={"onFinish"}
            onFinishFailed={"onFinishFailed"}
            autoComplete="off"
            ref={NewsForm}
          >
            <Form.Item
              label="新聞標題"
              name="title"
              rules={[{ required: true, message: 'Please input your username!' }]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="新聞分類"
              name="categoryId"
              rules={[{ required: true, message: 'Please input your username!' }]}
            >
              <Select>
                {
                  categoryList.map(item =>
                    <Option value={item.id} key={item.id} >{item.title}</Option>
                  )
                }
              </Select>
            </Form.Item>

          </Form>
        </div>
      </div>

      <div className={current === 1 ? '' : style.active}>
        <NewsEditor style={{}} getContent={(value)=>{
          console.log(value)
          setContent(value)
        }}/>
      </div>

      <div className={current === 2 ? '' : style.active}>
    
      </div>

      <div style={{ display: "flex", marginTop: "50px" }}>
        {
          current === 2 && <span>
            <Button type='="primary' onClick={()=>handleSave(0)}>保存草稿</Button>
            <Button danger onClick={()=>handleSave(1)}>提交審核</Button>
          </span>
        }
        {
          current > 0 && <Button type="primary" onClick={handlePrevious}>上一步</Button>
        }
        <div style={{ padding: 10 }}></div>
        {
          current < 2 && <Button type="primary" onClick={handleNext}>下一步</Button>
        }
      </div>
    </div>
  )
}
