import { Button, Table, Modal, notification } from 'antd'
import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { DeleteOutlined, EditOutlined, UploadOutlined, ExclamationCircleOutlined } from '@ant-design/icons';
import { Link, useNavigate } from 'react-router-dom';

const { confirm } = Modal

export default function NewsDraft() {
  let navigate = useNavigate()
  const [dataSource, setdataSource] = useState([])
  const { username } = JSON.parse(localStorage.getItem("token"))
  useEffect(() => {
    axios.get(`/news?author=${username}&auditState=0&_expand=category`).then(res => {
      const list = res.data
      setdataSource(list)
    })
  }, [username])

  const confirmMethod = (item) => {
    // console.log(item)
    confirm({
      title: '確定刪除嗎?',
      icon: <ExclamationCircleOutlined />,
      content: '該操作不可挽回!',
      onOk() {
        deleteMethod(item)
        // console.log('OK');
      },
      onCancel() {
      },
    });
  }
  const deleteMethod = (item) => {
    setdataSource(dataSource.filter(data => data.id !== item.id))
    axios.delete(`/news/${item.id}`)
  }

  const columns = [
    {
      title: 'ID',
      dataIndex: 'id',
      render: (id) => {
        return <div>{id}</div>
      }
    },
    {
      title: '新聞標題',
      dataIndex: 'title',
      render: (title,item) => {
        return <Link to={`/news-manage/preview/${item.id}`}>{title}</Link>
      }
    },
    {
      title: '作者',
      dataIndex: 'author',
    },
    {
      title: '分類',
      dataIndex: 'category',
      render: (category) => {
        return category.title
      }
    },
    {
      title: "操作",
      render: (item) => {

        return <div style={{ display: 'flex', flexDirection: 'row' }}>
          <Button danger shape="circle" icon={<DeleteOutlined />}
            onClick={() => confirmMethod(item)} />
          <Button shape="circle" icon={<EditOutlined />} 
          onClick={()=>{
            navigate(`/news-manage/update/${item.id}`)
          }}
          />

          <Button type="primary" shape="circle" icon=
            {<UploadOutlined />} onClick={()=>handleCheck(item.id)}/>
        </div>
      }
    }
  ];

  const handleCheck = (id) =>{
    // console.log(id)
    axios.patch(`/news/${id}`,{
      auditState:1
    }).then(res=>{
      navigate('/audit-manage/list')
      notification.info({
        message:`通知`,
        description:'你可以到'+'審核列表'+'中查看您的新聞',
        placement:"bottomRight",
      })
    })
  }

  return (
    <div>
      <Table dataSource={dataSource} columns={columns}
        rowKey={(item) => item.id}
        pagination={{
          pageSize: 5
        }}
      />
    </div>
  )
}
