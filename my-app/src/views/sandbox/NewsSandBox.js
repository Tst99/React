import React, { useEffect, useState } from "react";
import { BrowserRouter, Navigate, Route, Routes, useNavigate } from 'react-router-dom'
import SideMenu from "../../components/sandbox/SideMenu";
import TopHeader from "../../components/sandbox/TopHeader";
import Home from "./home/Home";
import Nopermission from "./nopermission/Nopermission";
import RightList from "./right-manage/RightList";
import RoleList from "./right-manage/RoleList";
import UserList from "./user-manage/UserList";
import NewsRouter from "../../components/sandbox/NewsRouter";

import NProgress from "nprogress"
import "nprogress/nprogress.css"

//css
import "./NewsSandBox.css"

//antd
import { Layout } from "antd";
const { Content } = Layout;


export default function NewsSandBox() {
    // const navigate = useNavigate()
    // const [iscollapsed,setisCollapsed] = useState(false)

    // console.log("NewsSandBox")

    NProgress.start()

    useEffect(()=>{
        NProgress.done()
    })

    // const handleCollapsed=(value)=>{
    //     setisCollapsed(value)
    // }
    return (
        <Layout>
            <SideMenu></SideMenu>
            <Layout className="site-layout">
                <TopHeader></TopHeader>
                <Content
                    className="site-layout-background"
                    style={{
                        margin: '24px 16px',
                        padding: 24,
                        minHeight: 280,
                        overflow: 'auto'
                    }}
                >
                    <NewsRouter></NewsRouter>
                </Content>
            </Layout>
        </Layout>
    )
}