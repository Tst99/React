import { Table, Button, Modal, Tree, Switch, Form, Input, Select } from 'antd'
import React, { useEffect, useState, ref } from 'react'
import axios from 'axios'
import { DeleteOutlined, EditOutlined, ExclamationCircleOutlined } from '@ant-design/icons';
import Item from 'antd/lib/list/Item';
import UserForm from '../../../components/user-manage/UserForm';
import { useNavigate } from "react-router-dom";
const { confirm } = Modal
const { Option } = Select;

export default function UserList() {
  let navigate = useNavigate()
  const [dataSource, setdataSource] = useState([])
  const [isAddVisible, setisAddVisible] = useState(false)
  const [isUpdateVisible, setisUpdateVisible] = useState(false)
  const [roleList, setroleList] = useState([])
  const [regionList, setregionList] = useState([])
  const [current, setcurrent] = useState(null)
  const [isUpdateDisabled, setisUpdateDisabled] = useState(false)
  const addForm = React.useRef(null)
  const updateForm = React.useRef(null)


  if (JSON.parse(localStorage.getItem("token")) == null){
    navigate("/login")
  }
  // const roleObj = {
  //   "1":"superadmin",
  //   "2":"admin",
  //   "3":"editor"
  // }
  useEffect(() => {
    let { roleId, region, username } = JSON.parse(localStorage.getItem("token"))
 
    const roleObj = {
      "1": "superadmin",
      "2": "admin",
      "3": "editor"
    }
    axios.get("/users?_expand=role").then(res => {
      const list = res.data
      setdataSource(roleObj[roleId] === "superadmin" ? list : [
        ...list.filter(item => item.username === username),
        ...list.filter(item => item.region === region && roleObj[item.roleId] === "editor")
      ])
    })
  }, [])

  useEffect(() => {
    axios.get("/regions").then(res => {
      const list = res.data
      setregionList(list)
    })
  }, [])

  useEffect(() => {
    axios.get("/roles").then(res => {
      const list = res.data
      setroleList(list)
    })
  }, [])

  const columns = [
    {
      title: '區域',
      dataIndex: 'region',
      filters: [
        ...regionList.map(item => ({
          text: item.title,
          value: item.value
        })),
        {
          text: "全球",
          value: "全球"
        }
      ],
      onFilter: (value, item) => {
        if (value === "全球") {
          return item.region === ""
        }
        return item.region === value
      },
      render: (region) => {
        return <b>{region === "" ? "全球" : region}</b>
      }
    },
    {
      title: '角色名稱',
      dataIndex: 'role',
      render: (role) => {
        return role?.roleName
      }
    },
    {
      title: '用戶名',
      dataIndex: 'username',
    },
    {
      title: '用戶狀態',
      dataIndex: 'roleState',
      render: (roleState, item) => {
        return <Switch checked={roleState} disabled={item.default} onChange={() => handleChange(item)}></Switch>
      }
    },
    {
      title: "操作",
      render: (item) => {
        return <div>
          <Button danger shape="circle" onClick={() => confirmMethod(item)} icon={<DeleteOutlined />} disabled={item.default} />
          <Button type="primary" shape="circle" icon={<EditOutlined />} disabled={item.default} onClick={() => handleUpdate(item)} />
        </div>
      }
    }
  ]

  const handleUpdate = (item) => {
    console.log(item)
    setTimeout(() => {
      if (item.roleId == 1) {
        setisUpdateDisabled(true)
      } else {
        setisUpdateDisabled(false)
      }
      updateForm.current.setFieldsValue(item)
    }, 0)

    setisUpdateVisible(true)
    setcurrent(item)

  }

  const handleChange = (item) => {
    console.log(item)
    item.roleState = !item.roleState
    setdataSource([...dataSource])
    axios.patch(`/users/${item.id}`, {
      roleState: item.roleState
    })
  }

  const confirmMethod = (item) => {
    confirm({
      title: '確定刪除嗎?',
      icon: <ExclamationCircleOutlined />,
      content: '該操作不可挽回!',
      onOk() {
        deleteMethod(item)
        // console.log('OK');
      },
      onCancel() {
      },
    });
  }

  const deleteMethod = (item) => {
    // console.log(item)
    // console.log(dataSource)
    setdataSource(dataSource.filter(data => data.id != item.id))
    axios.delete(`/users/${item.id}`)
  }


  function addFormOK() {
    console.log("add", addForm)
    addForm.current.validateFields().then(async (value) => {
      // console.log(value)
      setisAddVisible(false)
      addForm.current.resetFields()
      await axios.post(`/users`, {
        ...value,
        "roleState": true,
        "default": false,
      }).then(res => {
        console.log(res.data)
        setdataSource([...dataSource, {
          ...res.data,
          role: roleList.filter(item => item.id === value.roleId)[0]
        }])

      })
      // await axios.get("/users?_expand=role").then(res => {
      // const list = res.data
      // console.log(list)
      // setdataSource(list)})
    }).catch(err => {
      console.log(err)
    })
  }

  const UpdateFormOK = () => {
    updateForm.current.validateFields().then(value => {
      console.log(value)
      setisUpdateVisible(false)
      setdataSource(dataSource.map(item => {
        if (item.id === current.id) {
          return {
            ...item,
            ...value,
            role: roleList.filter(data => data.id === value.roleId)[0]
          }
        }
        return item
      }))
      setisUpdateDisabled(!isUpdateDisabled)
      axios.patch(`/users/${current.id}`,
        value)
    })
  }

  return (
    <div>
      <Button type='primary' onClick={() => {
        setisAddVisible(true)
      }}>添加用戶</Button>

      <Table dataSource={dataSource} columns={columns} pagination={{ pageSize: 5 }}
        rowKey={item => item.id} />

      <Modal
        visible={isAddVisible}
        title="添加用戶"
        okText="確定"
        cancelText="取消"
        onCancel={() => { setisAddVisible(false) }}
        onOk={() => addFormOK()}
      >
        <UserForm regionList={regionList} roleList={roleList} ref={addForm}></UserForm>
      </Modal>

      <Modal
        visible={isUpdateVisible}
        title="更新用戶"
        okText="更新"
        cancelText="取消"
        onCancel={() => {
          setisUpdateVisible(false)
          setisUpdateDisabled(!isUpdateDisabled)
        }}
        onOk={() => UpdateFormOK()}
      >
        <UserForm regionList={regionList} roleList={roleList} ref={updateForm} isUpdateDisabled={isUpdateDisabled} isUpdate={true}></UserForm>
      </Modal>
    </div>
  )
}
