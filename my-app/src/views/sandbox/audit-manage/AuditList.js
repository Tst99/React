import axios from 'axios'
import { Table ,Button,Tag,notification} from 'antd'
import React, { useEffect, useState } from 'react'
import { Link, useNavigate } from 'react-router-dom';




export default function AuditList() {
  const navigate =useNavigate()
  const [dataSource, setdataSource] = useState([])
  const { username } = JSON.parse(localStorage.getItem('token'))
  useEffect(() => {
    axios.get(`/news?author=${username}&auditState_ne=0&publishState_lte=1&_expand=category`).then(res => {
      console.log(res.data)
      setdataSource(res.data)
    })
  }, [username])

  const columns = [
    {
      title: '新聞標題',
      dataIndex: 'title',
      render: (title,item) => {
        return <Link to={`/news-manage/preview/${item.id}`}>{title}</Link>
      }
    },
    {
      title: '作者',
      dataIndex: 'author',
    },
    {
      title: '新聞分類',
      dataIndex: 'category',
      render: (category) => {
        return <div>{category.title}</div>
      }
    },
    {
      title: '審核狀態',
      dataIndex: 'auditState',
      render: (auditState) => {
        const colorList = ["","orange","green","red"]
        const auditList = ["草稿箱"," 審核中" ,"已通過","未通過"]
        return <Tag color={colorList[auditState]}>{auditList[auditState]}</Tag>
      }
    },
    {
      title: "操作",
      render: (item) => {
        return <div style={{ display: 'flex', flexDirection: 'row' }}>
          {item.auditState===1 && <Button onClick={()=>handleRevert(item)} >撤銷</Button>}
          {item.auditState===2 && <Button danger onClick={()=>handlePublish(item)} >發佈</Button>}
          {item.auditState===3 && <Button onClick={()=>handleUpdate(item)} type="primary" >更新</Button>}
          
        </div>
      }
    }
  ];
  
  const handleUpdate = (item) =>{
    navigate(`/news-manage/update/${item.id}`)
  }

  const handlePublish = (item) =>{
    setdataSource(dataSource.filter(data=>data.id!==item.id))
    axios.patch(`/news/${item.id}`,{
      publishState: 2,
      publishTime: Date.now()
    }).then(res=>{
      navigate(`/publish-manage/published`)
      notification.info({
        message:`通知`,
        description:'你可以到'+'【發佈管理/已經發佈】'+'中查看你的新聞',
        placement:"bottomRight",
      })
    })
  }
  
  const handleRevert = (item) => {
    setdataSource(dataSource.filter(data=>data.id!==item.id))
    // console.log(item)
    axios.patch(`/news/${item.id}`,{
      auditState:0
    }).then(res=>{
      notification.info({
        message:`通知`,
        description:'你可以到'+'草稿箱'+'中查看你的新聞',
        placement:"bottomRight",
      })
    })
  }

  return (
    <div>
      <Table dataSource={dataSource} columns={columns}
        pagination={{
          pageSize: 5
        }}
        rowKey={Item => Item.id}
      />
    </div>
  )
}

