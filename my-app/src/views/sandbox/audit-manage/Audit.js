import React,{useState,useEffect} from 'react'
import axios from 'axios'
import { Table,Tag,Button, notification } from 'antd'
import { Link } from 'react-router-dom'
import AuditList from './AuditList'

export default function Audit() {
  const [dataSource, setdataSource] = useState([])
  let { roleId, region, username } = JSON.parse(localStorage.getItem("token"))

  const handleAudit=(item,auditState,publishState)=>{
    setdataSource(dataSource.filter(data=>data.id !== item.id))
    axios.patch(`/news/${item.id}`,{
      auditState:auditState,
      publishState:publishState
    }).then(res=>{
      notification.info({
        message:`通知`,
        description:'你可以到'+'【審核管理/審核列表】'+'中查看你的新聞',
        placement:"bottomRight",
      })
    })
  }

  const columns = [
    {
      title: '新聞標題',
      dataIndex: 'title',
      render: (title,item) => {
        return <Link to={`/news-manage/preview/${item.id}`}>{title}</Link>
      }
    },
    {
      title: '作者',
      dataIndex: 'author',
    },
    {
      title: '新聞分類',
      dataIndex: 'category',
      render: (category) => {
        return <div>{category.title}</div>
      }
    },
    {
      title: "操作",
      render: (item) => {
        return <div style={{ display: 'flex', flexDirection: 'row' }}>
          <Button shape='circle' type='primary' onClick={()=>handleAudit(item,2,1) }><img style={{position:"relative",top:"-4px",width:"30px"}} src='/logo/tick.png'></img></Button>
          <div style={{padding:"0px 5px"}}></div>
          <Button shape='circle' danger onClick={()=>handleAudit(item,3,0)}><img style={{position:"relative",top:"-4px",width:"30px"}} src='/logo/cross.png'></img></Button>
        </div>
      }
    }
  ];

  useEffect(()=>{
    const roleObj ={
      "1":"superadmin",
      "2":"admin",
      "3":"editor"
    }
    axios.get(`/news?auditState=1&_expand=category`).then(res=>{
      const list = res.data
      // console.log(res.data)
      // setdataSource([...res.data])
      setdataSource(roleObj[roleId]==="superadmin"?list:[
        ...list.filter(item=>item.author===username),
        ...list.filter(item=>item.region===region && roleObj[item.roleId]==="editor")
      ])
    })
  },[roleId, region, username])


  return (
    <div>
      <Table dataSource={dataSource} columns={columns}
        pagination={{
          pageSize: 5
        }}
        rowKey={Item => Item.id}
      />
    </div>
  )
}
