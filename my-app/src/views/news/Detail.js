import React, { useEffect, useState } from 'react'
import { Button, Descriptions, PageHeader } from 'antd';
import { useMatch } from 'react-router-dom';
import moment from 'moment'
import axios from 'axios';
import {LikeOutlined} from "@ant-design/icons"


export default function Detail(props) {
  const match = useMatch('/detail/:id')
  const [newsInfo, setnewsInfo] = useState(null)

  useEffect(() => {
      // console.log(match.params.id)
      axios.get(`/news/${match.params.id}?&_expand=category&_expand=role`).then(res => {
          // console.log(res.data)
          setnewsInfo({
            ...res.data,
            view:res.data.view+1
          })
          return res.data
        }).then(res=>{
          axios.patch(`/news/${match.params.id}`,{
            view:res.view+1
          })
        })

  }, [match.params.id])
  
  const handleLike = ()=>{
    setnewsInfo({
      ...newsInfo,
      star:newsInfo.star+1
    })

    axios.patch(`/news/${match.params.id}`,{
      star:newsInfo.star+1
    })
  }


  return (
      <div>
          {
              newsInfo && <div>
                  <PageHeader
                      // ghost={true}
                      onBack={() => window.history.back()}
                      title={newsInfo.title}
                      subTitle={<div>
                        {newsInfo.category.title}
                        <LikeOutlined style={{padding:"10px"}} onClick={()=> handleLike()}/>
                      </div>}
                  >
                      <Descriptions size="small" column={3}>
                          <Descriptions.Item label="創建者">
                              {newsInfo.author}
                          </Descriptions.Item>

                          {/* <Descriptions.Item label="創建時間">
                              {moment(newsInfo.createTime).format("YYYY/MM/DD HH:mm:ss")}
                          </Descriptions.Item> */}

                          <Descriptions.Item label="發佈時間">
                              {newsInfo.pulishTime?moment(newsInfo.createTime).format("YYYY/MM/DD HH:mm:ss"):'-'}
                          </Descriptions.Item>

                          <Descriptions.Item label="區域">
                              {newsInfo.region}
                          </Descriptions.Item>

                          {/* <Descriptions.Item label="審核狀態" contentStyle={{color:colorList[newsInfo.auditState]}}>
                              {auditList[newsInfo.auditState]}
                          </Descriptions.Item>

                          <Descriptions.Item label="發佈狀態" contentStyle={{color:colorList[newsInfo.publishState]}}>
                              {publishList[newsInfo.publishState]}
                          </Descriptions.Item> */}

                          <Descriptions.Item label="訪問數量">
                              {newsInfo.view}
                          </Descriptions.Item>

                          <Descriptions.Item label="點讚數量">
                              {newsInfo.star}
                          </Descriptions.Item>

                          <Descriptions.Item label="評論數量">
                              0
                          </Descriptions.Item>
                      </Descriptions>
                  </PageHeader>

                  <div dangerouslySetInnerHTML={{
                      __html:newsInfo.content
                  }} style={{
                      margin:"0 24px",
                      border:'1px solid black'
                  }}>
                  </div>
              </div>
          }
      </div>
  )
}

