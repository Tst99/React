import axios from 'axios'
import { use } from 'echarts'
import React, { useEffect, useState } from 'react'
import { PageHeader, Row, Col, Card, List } from "antd"
import _ from "lodash"
import { Link } from 'react-router-dom'
import moment from 'moment'

export default function News() {

    const [list, setlist] = useState([])

    useEffect(() => {
        axios.get("/news?publishState=2&_expand=category").then(res => {
            // console.log(res.data)
            console.log(Object.entries(_.groupBy(res.data, item => item.category.title))[3][1].sort(function(x,y){
                return x.publishTime - y.publishTime
            }))
            let datelist = Object.entries(_.groupBy(res.data, item => item.category.title))
            for(const x in datelist){
                datelist[x][1].sort(function(x,y){
                    return y.publishTime - x.publishTime
                })
            }
            setlist(datelist)
            
        })
    }, [])

    return (
        <div style={{
            width: "95%",
            margin: "0 auto"
        }}>
            <PageHeader
                className="site-page-header"
                title="全球新聞"
                subTitle="查看新聞"
            />

            <div >
                <Row gutter={[16, 16]}>
                    {
                        list.map(item =>
                            <Col span={8} key={item[0]}>
                                <Card title={item[0]} bordered={true} hoverable={true}>
                                    <List
                                        size='small'
                                        dataSource={item[1]}
                                        pagination={{
                                            pageSize: 3
                                        }}
                                        renderItem={data => <List.Item><Link to={`/detail/${data.id}`}>{data.title}</Link>
                                            <div>{data.publishTime?moment(data.publishTime).format("YYYY/MM/DD"):"-"}</div>
                                        </List.Item>}
                                    ></List>
                                </Card>
                            </Col>
                        )
                    }
                </Row>
            </div>

        </div>
    )
}

