import React, { useEffect, useState } from "react";
import { Form, Button, Input, Checkbox, message, Spin } from "antd"
import { UserOutlined, LockOutlined, AlibabaOutlined, LoadingOutlined } from '@ant-design/icons';
import "./Login.css"
import Particles from "react-tsparticles";
import { loadFull } from "tsparticles";
import { tsParticles } from "tsparticles-engine";
import jsonfile from "./particles.json"
import axios from "axios"
import { useNavigate } from "react-router-dom";

export default function Login(props) {
    const [loading, setloading] = useState(true)
    let navigate = useNavigate()
    const [visibility, setvisibility] = useState("hidden")
    const [loginning, setloginning] = useState(false)


    const antIcon = <LoadingOutlined style={{ fontSize: 60, color: "black", zIndex: 101 }} spin />;

    useEffect(() => {
        setloading(false)
        setvisibility("visible")

        if (JSON.parse(localStorage.getItem("token"))) {
            navigate('/home')
        }
    }, [])

    console.log('login')
    const onFinish = (values) => {
        // console.log(values)
        setloginning(true)
        axios.get(`/users?username=${values.username}&password=${values.password}&roleState=true&_expand=role`).then(res => {
            // console.log(res.data)
            if (res.data.length == 0) {
                message.error("Invalid username or password")
                setloginning(false)
            } else {
                localStorage.setItem("token", JSON.stringify(res.data[0]))
                // window.location.reload(true);
                props.rend()
                navigate("/home", { replace: true })

                // console.log(localStorage.getItem("token"))
            }
        })
    }

    const particlesInit = async (main) => {
        // console.log(main);
        await loadFull(tsParticles);
    };
    const particlesLoaded = (container) => {
        // console.log("container",container);
    };

    return (
        <Spin size='large' spinning={loginning}>
            <div style={{ background: 'rgba(35,39,65,0.834)', height: "100%" }}>

                <div className="loadingContainer" style={{ top: "48%", left: "48%" }}>
                    <Spin indicator={antIcon} spinning={loginning} tip="Validating" style={{ color: "red", fontSize: "20px" }}></Spin>
                    <Spin indicator={antIcon} spinning={loading} tip="Loading" />
                </div>

                <Particles id="tsparticles"
                    init={particlesInit}
                    loaded={particlesLoaded}
                    options={jsonfile}
                />

                <div className="formContainer" style={{ flex: 1, visibility: visibility }}>
                    <div className="logintitle"><AlibabaOutlined /> 新聞發佈管理系統</div>
                    <Form
                        name="normal_login"
                        className="login-form"
                        initialValues={{
                            remember: true,
                        }}
                        onFinish={onFinish}
                    >
                        <Form.Item
                            name="username"
                            rules={[
                                {
                                    required: true,
                                    message: 'Please input your Username!',
                                },
                            ]}
                        >
                            <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Username" />
                        </Form.Item>
                        <Form.Item
                            name="password"
                            rules={[
                                {
                                    required: true,
                                    message: 'Please input your Password!',
                                },
                            ]}
                        >
                            <Input
                                prefix={<LockOutlined className="site-form-item-icon" />}
                                type="password"
                                placeholder="Password"
                            />
                        </Form.Item>
                        <Form.Item >
                            <div className="big" style={{ display: "flex" ,flexDirection:"row",justifyContent:"space-between"}}>
                                <div style={{ width:"80px" }}></div>
                                <Button type="primary" htmlType="submit" className="login-form-button" style={{width:"100px"}}>
                                    Log in
                                </Button>
                                
                                <Button onClick={()=>{
                                    navigate("/news")
                                }}>Visitor Page</Button>
                            </div>
                        </Form.Item>

                    </Form>
                </div>
            </div>
        </Spin>
    )
}