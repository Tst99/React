import React, { useEffect, useState } from 'react'
import { BrowserRouter, Route, Routes, Navigate } from 'react-router-dom'

import Login from '../views/login/Login'
import Detail from '../views/news/Detail'
import News from '../views/news/News'
import NewsSandBox from '../views/sandbox/NewsSandBox'


export default function IndexRouter() {
    const[render,rerender] = useState(true)

    function rend() {
        rerender(!render)
    }
    return (
        <BrowserRouter>
            <Routes>
                <Route path='/news' element={<News/>}></Route>
                <Route path='/detail/:id' element={<Detail/>}></Route>
                <Route path="login" element={<Login rend={()=>rend()}/>} />
                <Route path="*" element={
                    localStorage.getItem("token") ? <NewsSandBox /> : <Navigate to="login" replace={false} />
                } />
            </Routes>
        </BrowserRouter>
    )
}

